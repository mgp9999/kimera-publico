#ifdef __linux__
#define REMOVE "rm "
#endif // __linux__

#ifdef __WIN32__
#define REMOVE "del "
#endif // _WINDOWS

#ifdef __WIN64__
#define REMOVE "del "
#endif // _WINDOWS

#ifdef __APPLE__
#define REMOVE "rm "
#endif // _APPLE


#include "Simulation.h"

Simulation::Simulation()
{

}

Simulation::~Simulation()
{
    //dtor
}

void Simulation::update_surface_and_events(Atom * dissolved_atom,Box *box, Event_stack * eventstack, Control *control)
{
    long long int size=dissolved_atom->get_size_neighbour();
    for (long long int i=0;i<size;i++)
    {
        if (!box->check_in_surface(dissolved_atom->get_id_neighbour(i)))
        {
            box->add_surface_atom(dissolved_atom->get_neighbour(i));
            eventstack->add_event(dissolved_atom->get_neighbour(i), control);
        }
    }
}

void Simulation::update_surface_and_events_complex(Atom * dissolved_atom,Box *box, Event_stack * eventstack, Control *control)
{
    vector<Atom*> neighbors = dissolved_atom->get_neighbours();
    long long int sizeneighbors=neighbors.size();

    //remove the atom and modify the neighbors
    long long int dissolved_type= dissolved_atom->get_type();

    box->rm_atom_complex(dissolved_atom->get_id());

    vector<Event_definition*> list_definition=control->get_list_event_definition();
    long long int size_list_definition=list_definition.size();

    if (dissolved_type==NORMAL)
    {
        #pragma omp parallel for
        for (long long int i=0;i<sizeneighbors;i++)
        {
            Atom * targetatom=neighbors.at(i);

            vector<Atom*>  neighbours_involvedatom= targetatom->get_neighbours();
            vector<double>  distances_involvedatom= targetatom->get_distances_to_neighbours();

            if (!targetatom->get_insurface() && targetatom->get_type()==NORMAL) //if not in surface and normal
            {
                //we check if is still a bulk atom (the neighbor we want to enter in surface)

                for (long long int j=0;j<size_list_definition;j++)
                {
                    if (list_definition.at(j)->get_involved_atom_type().compare(targetatom->get_atom_type())==0)
                    {

                        vector<Linked_neighbour> linkvector=list_definition.at(j)->get_list_linked_neighbour();

                        bool bulk=true;
                        bulk=targetatom->is_bulk_with_links(list_definition.at(j)->get_max_to_bulk(),
                                                 list_definition.at(j)->get_type_neighbours(),list_definition.at(j)->get_distance_neighbours(),
                                                 linkvector);



                        if (!bulk)
                        {
                            #pragma omp critical
                            {
                                box->add_surface_atom(targetatom);
                                //targetatom->set_insurface(true); //no need, add_surface_atom do it for you
                                eventstack->add_event_complex(targetatom, control);

                            }
                            break;
                        }
                    }
                }
            }
        }
    }
}


//Obsolete method, not used
long long int Simulation::dissolve_atom_by_position_event( long long int position, Box * box, Event_stack * eventstack, Control * control)
{
    //First we put the atoms in surface and their corresponding events
    Atom * dissolved_atom=eventstack->get_event_from_stack(position)->get_involved_atoms().at(0);
    update_surface_and_events(dissolved_atom, box, eventstack,  control);


    //we delete the event corresponding to the dissolved atom
    eventstack->rm_event_by_position(position);

    //And then we delete the atom (its neighbors see its updated neighbor number, but not its rate).
    if(box->rm_atom(dissolved_atom->get_id())!=0) {return 1;}

    // And finally, we update the rate of the neighboring atoms and the propensity
    eventstack->update_stackrate_propensity(control);

    return 0;
}


long long int Simulation::dissolve_atom_by_position_event_complex( long long int position, Box * box, Event_stack * eventstack, Control * control, Tracker * tracker)
{
    //First we put the atoms in surface and their corresponding events
    Event * chosen_event=eventstack->get_event_from_stack(position);

    long long int size_involved_atoms=chosen_event->get_involved_atoms().size();

    Atom * dissolved_atom;

    //Do not paralelize
    for (long long int i=0;i<size_involved_atoms;i++)
    {
            dissolved_atom=chosen_event->get_involved_atoms().at(i);

            //We enter it in the tracker if its type is normal (if it was not dissolved yet).

            if (dissolved_atom->get_type()==NORMAL) tracker->add_atom_dissolved(dissolved_atom);

            //Here we still have the information of the event

            update_surface_and_events_complex(dissolved_atom, box, eventstack,  control);

    }

    //we delete the event corresponding to the dissolved atom
    eventstack->rm_event_by_position(position);

    //And finally, we update the rate of the neighboring atoms and the propensity

    eventstack->update_stackrate_propensity_complex();

    return 0;
}


long long int Simulation::KMC_step_complex(Event_stack * mainstack, Control * control, Box * box, Tracker * tracker)
{
    //If the event size of the stack is 0... we have nothing more to do...
    if (mainstack->get_stack_size()==0) return 1; //fail

    double propensity;
    //we import the random number initializer
    mt19937 &mt = RandomGenerator::Instance().get();
    auto seed = std::chrono::high_resolution_clock::now().time_since_epoch().count();
    mt.seed(seed);
    uniform_real_distribution<double> dist01(0.0, 1.0);
    long long int mainpos;
    double kmctime;

    if (control->get_unstuckalert()==control->get_unstuckeverystep())
    {
        Event_stack_unstuck unstuckstack(IDUNSTUCKSTACK);
        //Once created, we create the appropriate events
        unstuckstack.set_unstuck_events_complex(mainstack, control);
        //if the size of the unstuckstack is 0... bad thing.
        if (unstuckstack.get_stack_size()==0) {
            cout<< "unstuckstack without elements!, try to reduce TARGET_TIME or/and increase ACCURACY"<<endl;
            control->restart_unstuckalert();
            //re-entering the method
            KMC_step_complex( mainstack, control, box, tracker);

        }
        //we set the propensity for each event and the total
        unstuckstack.set_stackpropensity();
        //we generate the random number according to the propensity of the stack...
        double unstuckstackpropensity= unstuckstack.get_stackpropensity();
        propensity=unstuckstackpropensity;

        uniform_real_distribution<double> dist(0.0, unstuckstackpropensity);

        //we search according to the value in the unstuck stack
        long long int unstuckpos;

        if (control->get_linealsearch()) {unstuckpos=unstuckstack.event_lineal_search(dist(mt));}
        else {unstuckpos=unstuckstack.event_binary_search(dist(mt));}

        //we look at the corresponding position in the main stack
        mainpos=unstuckstack.get_correspondingposition(unstuckpos);
        //we look at the time the system should be increased according to the propensity of the unstuckstack
        // and a new random number between 0 and 1(which cannot be 0.0)
        double uprima=dist01(mt);
        if (uprima<EPSILON*EPSILON*EPSILON*EPSILON) uprima=EPSILON*EPSILON*EPSILON*EPSILON;
        kmctime=1.0/unstuckstackpropensity*log(1.0/uprima);

        //reset the alert
        control->restart_unstuckalert();
    }
    else
    {
        double mainstackpropensity= mainstack->get_stackpropensity();
        propensity=mainstackpropensity;

        uniform_real_distribution<double> dist(0.0, mainstackpropensity);

        //we look at the corresponding position in the main stack

        if (control->get_linealsearch()) {mainpos=mainstack->event_lineal_search(dist(mt));}
        else {mainpos=mainstack->event_binary_search(dist(mt));}

        //we look at the time the system should be increased according to the propensity of the mainstack
        //and a new random number between 0 and 1(which cannot be 0.0)
        double uprima=dist01(mt);
        if (uprima<EPSILON*EPSILON*EPSILON*EPSILON) uprima=EPSILON*EPSILON*EPSILON*EPSILON;
        kmctime=1.0/mainstackpropensity*log(1.0/uprima);

        //we increase the alert if the time is very small

        if (kmctime < control->get_unstuck_compare_time()) control->increaseby1_unstuckalert();
    }
    Event * chosen_event= mainstack->get_event_from_stack(mainpos);

    //the removal of a non coordinated atom, without time increment
    if (chosen_event->get_type()==WITH_OUT_NEIGH_TYPE)
    {
            control->increaseby1_eventsaccepted();
            dissolve_atom_by_position_event_complex(mainpos, box, mainstack, control,tracker);
            control->increaseby1_step();
            return 0;
    }

    //We need to increase the time and events accepted
    control->increment_time_by(kmctime);
    control->increaseby1_eventsaccepted();
    //we update the events and the box and surface, and rate
    dissolve_atom_by_position_event_complex(mainpos, box, mainstack, control,tracker);
    control->increaseby1_step();

    return 0;
}


long long int Simulation::KMC_step_complex(Event_stack * mainstack, Control * control, Box * box, Tracker * tracker,long long int seed)
{
    //If the event size of the stack is 0... we have nothing more to do...
    if (mainstack->get_stack_size()==0) return 1; //fail

    double propensity;
    //we import the random number initializer
    mt19937 &mt = RandomGenerator::Instance().get();
    mt.seed(seed);
    uniform_real_distribution<double> dist01(0.0, 1.0);
    long long int mainpos;
    double kmctime;

    if (control->get_unstuckalert()==control->get_unstuckeverystep())
    {
        Event_stack_unstuck unstuckstack(IDUNSTUCKSTACK);
        //Once created, we create the appropriate events
        unstuckstack.set_unstuck_events_complex(mainstack, control);
        //if the size of the unstuckstack is 0... bad thing.
        if (unstuckstack.get_stack_size()==0) {
            cout<< "unstuckstack without elements!, try to reduce TARGET_TIME or/and increase ACCURACY"<<endl;
            control->restart_unstuckalert();
            //re-entering the method
            KMC_step_complex( mainstack, control, box, tracker,seed);
        }

        //we set the propensity for each event and the total
        unstuckstack.set_stackpropensity();
        //we generate the random number according to the propensity of the stack...
        double unstuckstackpropensity= unstuckstack.get_stackpropensity();
        propensity=unstuckstackpropensity;

        uniform_real_distribution<double> dist(0.0, unstuckstackpropensity);
        //we search according to the value in the unstuck stack
        long long int unstuckpos;
        if (control->get_linealsearch()) {unstuckpos=unstuckstack.event_lineal_search(dist(mt));}
        else {unstuckpos=unstuckstack.event_binary_search(dist(mt));}
        //we look at the corresponding position in the main stack
        mainpos=unstuckstack.get_correspondingposition(unstuckpos);
        //we look at the time the system should be increased according to the propensity of the unstuckstack
        // and a new random number between 0 and 1(which cannot be 0.0)
        double uprima=dist01(mt);
        if (uprima<EPSILON*EPSILON*EPSILON*EPSILON) uprima=EPSILON*EPSILON*EPSILON*EPSILON;
        kmctime=1.0/unstuckstackpropensity*log(1.0/uprima);

        //reset the alert
        control->restart_unstuckalert();
    }
    else
    {
        double mainstackpropensity= mainstack->get_stackpropensity();
        propensity=mainstackpropensity;

        uniform_real_distribution<double> dist(0.0, mainstackpropensity);
        //we look at the corresponding position in the main stack
        if (control->get_linealsearch()) { mainpos=mainstack->event_lineal_search(dist(mt));}
        else { mainpos=mainstack->event_binary_search(dist(mt));}

        //we look at the time the system should be increased according to the propensity of the mainstack
        //and a new random number between 0 and 1(which cannot be 0.0)
        double uprima=dist01(mt);
        if (uprima<EPSILON*EPSILON*EPSILON*EPSILON) uprima=EPSILON*EPSILON*EPSILON*EPSILON;
        kmctime=1.0/mainstackpropensity*log(1.0/uprima);

        //we increase the alert if time increment is very small
        if (kmctime < control->get_unstuck_compare_time()) control->increaseby1_unstuckalert();
    }
    Event * chosen_event= mainstack->get_event_from_stack(mainpos);

    //removal of a non coordinated atom without time increment
    if (chosen_event->get_type()==WITH_OUT_NEIGH_TYPE)
    {
            control->increaseby1_eventsaccepted();
            dissolve_atom_by_position_event_complex(mainpos, box, mainstack, control,tracker);
            control->increaseby1_step();
            return 0;
    }
    //We need to increase the time and events accepted
    control->increment_time_by(kmctime);
    control->increaseby1_eventsaccepted();


        //we update, event, box, and surface, and rate
        dissolve_atom_by_position_event_complex(mainpos, box, mainstack, control,tracker);

    control->increaseby1_step();

    return 0;
}



//Obsolete method, not used
long long int Simulation::KMC_step(Event_stack * mainstack, Control * control, Box * box)
{
    if (mainstack->get_stack_size()==0) return 1;

    double propensity;

    mt19937 &mt = RandomGenerator::Instance().get();
    uniform_real_distribution<double> dist01(0.0, 1.0);
    long long int mainpos;
    double kmctime;

    if (control->get_unstuckalert()==control->get_unstuckeverystep())
    {
        Event_stack_unstuck unstuckstack(IDUNSTUCKSTACK);

        unstuckstack.set_unstuck_events(mainstack, control);

        unstuckstack.set_stackpropensity();

        double unstuckstackpropensity= unstuckstack.get_stackpropensity();
        propensity=unstuckstackpropensity;

        //.....
        uniform_real_distribution<double> dist(0.0, unstuckstackpropensity);

        long long int unstuckpos;
        if (control->get_linealsearch()) {unstuckpos=unstuckstack.event_lineal_search(dist(mt));}
        else {unstuckpos=unstuckstack.event_binary_search(dist(mt));}

        mainpos=unstuckstack.get_correspondingposition(unstuckpos);

        double uprima=dist01(mt);
        if (uprima<EPSILON*EPSILON*EPSILON*EPSILON) uprima=EPSILON*EPSILON*EPSILON*EPSILON;
        kmctime=1.0/unstuckstackpropensity*log(1.0/uprima);

        control->restart_unstuckalert();
    }
    else
    {
        double mainstackpropensity= mainstack->get_stackpropensity();
        propensity=mainstackpropensity;

        uniform_real_distribution<double> dist(0.0, mainstackpropensity);

        if (control->get_linealsearch()) {mainpos=mainstack->event_lineal_search(dist(mt));}
        else {mainpos=mainstack->event_binary_search(dist(mt));}

        double uprima=dist01(mt);
        if (uprima<EPSILON*EPSILON*EPSILON*EPSILON) uprima=EPSILON*EPSILON*EPSILON*EPSILON;
        kmctime=1.0/mainstackpropensity*log(1.0/uprima);

        if (kmctime < control->get_unstuck_compare_time()) control->increaseby1_unstuckalert();
    }

    control->increment_time_by(kmctime);
    control->increaseby1_eventsaccepted();

    long long int vecinos = mainstack->get_involved_first_atom(mainpos)->get_size_neighbour();
    double ratioevento= mainstack->get_eventrate(mainpos);

    double ratiocaractreforma=Models::model42_reforming(control->get_fr(),vecinos,control->get_Er(),control->get_deltag());

    double tiempocaractdiss=-log(0.001)/(ratioevento);

    double probabilidad=1.0-exp(-ratiocaractreforma*tiempocaractdiss);

    double uprimaprima=dist01(mt);

    if (uprimaprima<probabilidad)
    {
        cout<<"--aqui no deberia llegar "<<endl;
        double uprima2=dist01(mt);

        if (uprima2<EPSILON*EPSILON*EPSILON*EPSILON) uprima2=EPSILON*EPSILON*EPSILON*EPSILON;

        double tiemporeformacion=1.0/(propensity-ratioevento+ratiocaractreforma)*log(1.0/uprima2);
        control->increment_time_by(tiemporeformacion);
    }
    else
    {
        dissolve_atom_by_position_event(mainpos, box, mainstack, control);
    }

    control->increaseby1_step();

    return 0;
}


//Obsolete method, not used
long long int Simulation::KMC_step_resolution(Event_stack * mainstack, Control * control, Box * box)
{

    if (mainstack->get_stack_size()==0) return 1;

    double propensity;

    mt19937 &mt = RandomGenerator::Instance().get();
    uniform_real_distribution<double> dist01(0.0, 1.0);
    long long int mainpos;
    double kmctime;

    if (control->get_unstuckalert()==control->get_unstuckeverystep())
    {
        Event_stack_unstuck unstuckstack(IDUNSTUCKSTACK);

        unstuckstack.set_unstuck_events(mainstack, control);

        unstuckstack.set_stackpropensity();

        double unstuckstackpropensity= unstuckstack.get_stackpropensity();
        propensity=unstuckstackpropensity;

        uniform_real_distribution<double> dist(0.0, unstuckstackpropensity);

        long long int unstuckpos;
        if (control->get_linealsearch()) {unstuckpos=unstuckstack.event_lineal_search(dist(mt));}
        else {unstuckpos=unstuckstack.event_binary_search(dist(mt));}
        mainpos=unstuckstack.get_correspondingposition(unstuckpos);

        double uprima=dist01(mt);
        if (uprima<EPSILON*EPSILON*EPSILON*EPSILON) uprima=EPSILON*EPSILON*EPSILON*EPSILON;
        kmctime=1.0/unstuckstackpropensity*log(1.0/uprima);

        control->restart_unstuckalert();
    }
    else
    {
        double mainstackpropensity= mainstack->get_stackpropensity();
        propensity=mainstackpropensity;

        uniform_real_distribution<double> dist(0.0, mainstackpropensity);

        if (control->get_linealsearch()) {mainpos=mainstack->event_lineal_search(dist(mt));}
        else {mainpos=mainstack->event_binary_search(dist(mt));}

        double uprima=dist01(mt);
        if (uprima<EPSILON*EPSILON*EPSILON*EPSILON) uprima=EPSILON*EPSILON*EPSILON*EPSILON;
        kmctime=1.0/mainstackpropensity*log(1.0/uprima);

        if (kmctime < control->get_unstuck_compare_time()) control->increaseby1_unstuckalert();
    }
    control->increment_time_by(kmctime*(control->get_time_modifier()));
    control->increaseby1_eventsaccepted();

    long long int vecinos = mainstack->get_involved_first_atom(mainpos)->get_size_neighbour();
    double ratioevento= mainstack->get_eventrate(mainpos);

    double ratiocaractreforma=Models::model42_reforming(control->get_fr(),vecinos,control->get_Er(),control->get_deltag());

    double tiempocaractdiss=-log(0.001)/(ratioevento);

    double probabilidad=1.0-exp(-ratiocaractreforma*tiempocaractdiss);

    double uprimaprima=dist01(mt);

    if (uprimaprima<probabilidad)
    {
        double uprima2=dist01(mt);

        if (uprima2<EPSILON*EPSILON*EPSILON*EPSILON) uprima2=EPSILON*EPSILON*EPSILON*EPSILON;

        double tiemporeformacion=1.0/(propensity-ratioevento+ratiocaractreforma)*log(1.0/uprima2);
        control->increment_time_by(tiemporeformacion*(control->get_time_modifier()));
    }
    else
    {
        dissolve_atom_by_position_event(mainpos, box, mainstack, control);
    }

    control->increaseby1_step();

    return 0;
}

long long int Simulation::complete_simulation(string name_input_file)
{
        cout << "Welcome to Kimera Program" << endl;

        string * pointer_name_input_file=& name_input_file;

        //We need: Initial ceel, data_printer, data_reader, control, box, event stack, and tracker
        Event_stack event_stack1=Event_stack(1);
        Event_stack * pointer_event_stack1= &event_stack1;

        Tracker tracker1= Tracker();
        Tracker * pointer_tracker1=& tracker1;

        Cell firstcell=Cell(-1);
        Cell * pointer_firstcell=& firstcell;

        Control control1=Control();
        Control * pointer_control1= & control1;

        Data_reader reader1=Data_reader();

        reader1.read_input_file(pointer_name_input_file,pointer_control1);


        if(!control1.get_parallelize())    {omp_set_num_threads(1);} // To remove paralelization
        else
        {
            long long int concurentThreadsSupported = thread::hardware_concurrency();
            long long int cores=control1.get_parallelizenumber();

            if (cores>concurentThreadsSupported)
            {
                cout<< endl <<"WARNING: You have asked for  " <<cores<< " cores, nevertheless the maximum number of cores available in your system are  "<< concurentThreadsSupported <<endl;
                cores=concurentThreadsSupported;
            }

            cout<< endl <<"The simulation is allocated in "<< cores <<" cores"<<endl<< endl;
            omp_set_num_threads(cores);
        }

        //we need work name
        Data_printer printer1(control1.get_workname());

        //we can create the box

        Box box1(control1.get_dimension_x(), control1.get_dimension_y(), control1.get_dimension_z(),
        control1.get_cell_a(), control1.get_cell_b(), control1.get_cell_c(),
        control1.get_angle_alpha(), control1.get_angle_beta(), control1.get_angle_gamma());

        Box * pointer_box1 = &box1;

        //If we have said that we read it from a kimera file...

        if (control1.get_kimerafile())
        {
            string file_kimera_box_name=control1.get_pathtokimerafile();
            string * pointer_file_kimera_box_name=& file_kimera_box_name;

            reader1.read_kimera_box(pointer_file_kimera_box_name,pointer_box1);
            box1.clean_gaps_before_start_complex_from_kimerafile();

            //We need to know the events

            reader1.read_input_file_dis_events(pointer_name_input_file,pointer_control1);

            //We set initial events

            event_stack1.set_initial_events_complex(pointer_box1,pointer_control1);
        }
        else
        {
            //It may happen that you have read an xyz file, and you want to add some new positions to the cell
            if (control1.get_xyzfile())
            {
                string file_xyz_name=control1.get_pathtoxyzfile();
                string * pointer_file_xyz_name=& file_xyz_name;

                reader1.read_create_xyz_file_without_corners(pointer_file_xyz_name,pointer_firstcell,pointer_control1);
            }

            //We read the cell to define the box
            reader1.read_input_file_cell(pointer_name_input_file,pointer_firstcell);
            //size of the position must be greater than 0 after reading it
            if (firstcell.get_cell_positions().size()==0){cout<<endl<< "Initial cell empty! " <<endl; exit(1);}


            //we create the box from the cell, see the seedboxbool
            if(control1.get_seedboxbool()) {box1.create_box_by_cell(pointer_firstcell,control1.get_seedbox());}
            else {box1.create_box_by_cell(pointer_firstcell);}

            //here we modify the elements of the box
            reader1.read_modify_element(pointer_name_input_file,pointer_box1);


            //Depending on the periodic conditions, the neighbors will be one or the other.
            box1.set_cell_neighbours_26(control1.get_plane_x(),control1.get_plane_y(),control1.get_plane_z());


            //leemos los eventos..son necesarios para definir los vecinos
            reader1.read_input_file_dis_events(pointer_name_input_file,pointer_control1);


            //we read the events...they are necessary to define neighbors
            vector <Event_definition*> events= control1.get_list_event_definition();
            long long int sizeevents=events.size();

            for (long long int i=0; i<sizeevents;i++)
            {

                if (control1.get_linked())
                {

                    box1.set_atom_neighbourhood_linked(events.at(i)->get_involved_atom_type(), events.at(i)->get_type_neighbours(), events.at(i)->get_distance_neighbours(), events.at(i)->get_list_linked_neighbour()) ;

                }
                else
                {
                    box1.set_atom_neighbourhood(events.at(i)->get_involved_atom_type(), events.at(i)->get_type_neighbours(), events.at(i)->get_distance_neighbours());
                }

                if (control1.get_affected())
                {
                    box1.set_atom_affected(events.at(i)->get_involved_atom_type(),
                       events.at(i)->get_type_affected(),
                       events.at(i)->get_distance_affected());
                }

            }

            //once we have all the neighbors, we define their record
            long long int sizebox=box1.get_box_atoms().size();
            #pragma omp parallel for
            for (long long int  i=0;i<sizebox;i++)
            {
                box1.get_box_atoms().at(i)->set_neigh_record();
            }

            //now that we have the box and its neighbors, let's modify it.
            reader1.read_input_file_topography(pointer_name_input_file,pointer_box1);


            //we free the empty spaces in the box. If it is different from 0, there has been an error in one of them.
            box1.clean_gaps_before_start_complex();


            //Now we define the initial surface

            box1.set_initial_surface_atoms(control1.get_list_event_definition());

            reader1.read_remove_add_from_surface(pointer_name_input_file,pointer_box1);

            //Now we define the initial events

            event_stack1.set_initial_events_complex(pointer_box1,pointer_control1);
        }


        //We define the atom mass

        reader1.read_input_file_mass(pointer_name_input_file,pointer_box1);


        //if we have said to estimate the time...

        if (control1.get_estimatetime())
        {

            vector<double> list_rates;

            bool invector=false;


            for (long long int h=0; h<(long long int)event_stack1.get_stack_size();h++)
            {
                for (long long int f=0; f<(long long int)list_rates.size(); f++)
                {
                    if (Util::isEqualhigh(event_stack1.get_eventrate(h),list_rates.at(f)))
                    {
                        invector=true;
                        break;
                    }
                }

                if (!invector)
                {
                    list_rates.push_back(event_stack1.get_eventrate(h));
                }
                invector=false;
            }

            sort(list_rates.begin(), list_rates.end());

            int pos=(int)(list_rates.size()/2.0);  //si coge el mas pequenno mejor, es el que mas tarda
            control1.set_targettime((double)box1.get_box_atoms().size()/(list_rates.at(pos)*(double)event_stack1.get_stack_size()));
            cout << endl<<"Estimated time for dissolution: "<<(double)box1.get_box_atoms().size()/(list_rates.at(pos)*(double)event_stack1.get_stack_size())<<" seconds"<<endl;

        }

        cout<<endl<<endl<<"You managed to reach this point!!!, Starting the simulation..."<<endl<<endl;

        //we print the results:

        if (control1.get_printinitialkimerafile()) printer1.print_initial_box(pointer_box1,"initial",control1.get_affected(), control1.get_linked());

        if (!control1.get_print_by_steps())
        {
            printer1.print_all_data(pointer_box1,pointer_control1,pointer_tracker1);

            while(control1.get_time()<control1.get_targettime())
            {
                    if  (control1.get_seedsimbool())
                    {
                         if(KMC_step_complex(pointer_event_stack1, pointer_control1,pointer_box1,pointer_tracker1,control1.get_seedsim())  !=0) break;
                    }
                    else
                    {
                        if(KMC_step_complex(pointer_event_stack1, pointer_control1,pointer_box1,pointer_tracker1)  !=0) break;
                    }

                    printer1.print_all_data(pointer_box1,pointer_control1,pointer_tracker1);
            }
        }

        else
        {
            printer1.print_all_data_step(pointer_box1,pointer_control1,pointer_tracker1);

            while(control1.get_step()<control1.get_targetsteps())
            {
                    if  (control1.get_seedsimbool())
                    {
                         if(KMC_step_complex(pointer_event_stack1, pointer_control1,pointer_box1,pointer_tracker1,control1.get_seedsim())  !=0) break;
                    }
                    else
                    {
                        if(KMC_step_complex(pointer_event_stack1, pointer_control1,pointer_box1,pointer_tracker1)  !=0) break;
                    }

                    printer1.print_all_data_step(pointer_box1,pointer_control1,pointer_tracker1);
            }
        }

        //print kimerabox if the system has something left and we have asked for it

        cout<<endl<<"Simulation finished!!"<<endl<<endl;

       if (control1.get_printfinalkimerafile()) printer1.print_initial_box(pointer_box1,"final",control1.get_affected(), control1.get_linked());

       cout<<endl<<"Compressing output files..."<<endl<<endl;
       //zip the output files

       string phrase="zip kimeraoutput.zip "+control1.get_workname()+".*";

        system(phrase.c_str());

        string phrase2=REMOVE+control1.get_workname()+".*";

        //if the file kimeraoutput.zip exists, we delete the rest of the files

        ifstream filezip("kimeraoutput.zip");

        if (filezip.is_open())
        {
            system(phrase2.c_str());
        }
        else
        {
            cout<<endl<<"Unable to zip the output files. You need zip command in your system!"<<endl<<endl;
        }

        return 0;
}
