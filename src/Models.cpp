#include "Models.h"

Models::Models()
{
    //ctor
}

Models::~Models()
{
    //dtor
}

//Obsolete method, not used
double Models::model42_dissolution(double fd, long long int neighbour, double Ed )
{
    double rate= fd*exp(-1.0*(double)neighbour*Ed);
    return rate;
}

//Obsolete method, not used
double Models::model42_reforming(double fr, long long int neighbour, double Er, double deltaG)
{
    double rate= fr *exp(-1.0*(Er*(double)neighbour-deltaG));
    return rate;
}

//Obsolete method, not used
double Models::model42_dissolution(double fd, vector <long long int> Nneighbour, vector <double> Eds )
{
    long long int sizeN=  Nneighbour.size();
    double exponent=0.0;

    for (long long int i = 0; i< sizeN; i++)
    {
        exponent+=(double)Nneighbour.at(i) * Eds.at(i);
    }

    double rate= fd*exp(-1.0*exponent);


    return rate;
}

//Obsolete method, not used
double Models::model42_reforming(double fr, vector <long long int> Nneighbour,  vector <double> Eps, double deltaG)
{
    long long int sizeN= Nneighbour.size();
    double exponent=0.0;

    for (long long int i = 0; i< sizeN; i++)
    {
        exponent+=(double)Nneighbour.at(i) * Eps.at(i);
    }

    double rate= fr *exp(-1.0*(exponent-deltaG));
    return rate;
}

//Obsolete method, not used
double Models::model42_dissolution(double fd, vector <long long int> Nneighbour, vector <double> Eds , vector<vector<double>> Eds_direct)
{
    long long int sizeN=  Nneighbour.size();
    double exponent=0.0;
    long long int size_Eds_direct=0;
    long long int j=0;


    for (long long int i = 0; i< sizeN; i++)
    {
        size_Eds_direct=Eds_direct.at(i).size();
        if (size_Eds_direct==0)
        {
            exponent+=(double)Nneighbour.at(i) * Eds.at(i);
        }
        else
        {

            j = Nneighbour.at(i)-1;
            if (Nneighbour.at(i)==0) {exponent+= 0.0; }
            else
            {
                if(j>=size_Eds_direct){cout << "Error! the direct definition do not cover the amount of neighbours, check the neighbour list"<<endl; exit(1); }


                exponent+=Eds_direct.at(i).at(j);
            }
        }

    }



    double rate= fd*exp(-1.0*exponent);


    return rate;
}

//Obsolete method, not used
double Models::model42_reforming(double fr, vector <long long int> Nneighbour,  vector <double> Eps, vector<vector<double>> Eps_direct, double deltaG)
{
    long long int sizeN= Nneighbour.size();
    double exponent=0.0;
    long long int size_Eps_direct=0;
    long long int j=0;

    for (long long int i = 0; i< sizeN; i++)
    {
        size_Eps_direct=Eps_direct.at(i).size();
        if (size_Eps_direct==0)
        {
            exponent+=(double)Nneighbour.at(i) * Eps.at(i);
        }
        else
        {
            j = Nneighbour.at(i)-1;
            if (Nneighbour.at(i)==0) {exponent+= 0.0; }
            else
            {
                if(j>=size_Eps_direct){cout << "Error! the direct definition do not cover the amount of neighbours, check the neighbour list"<<endl; exit(1);  }

                exponent+=Eps_direct.at(i).at(j);
            }
        }
    }

    double rate= fr *exp(-1.0*(exponent-deltaG));
    return rate;
}


double Models::model50_dissolution(double fd, double fr, vector <long long int> Nneighbour, vector <double> Eds , vector <double> Eps,  vector<vector<double>> Eds_direct, vector<vector<double>> Eps_direct ,double  deltaG)
{
    long long int sizeN=  Nneighbour.size();
    double exponentd=0.0;
    long long int size_Eds_direct=0;
    long long int j=0;

    for (long long int i = 0; i< sizeN; i++)
    {
        size_Eds_direct=Eds_direct.at(i).size();
        if (size_Eds_direct==0)  //if the size is 0, if we consider the linear model
        {
            exponentd+=(double)Nneighbour.at(i) * Eds.at(i);
        }
        else   //otherwise the position j of i is taken, where j is the number of neighbors-1 (Nneighbour.at(i)-1), it may happen that the number of neighbors is greater than the amount of values we have defined.
        {

            j = Nneighbour.at(i)-1;
            if (Nneighbour.at(i)==0) {exponentd+= 0.0; } //then the time is NOT increased
            else
            {
                if(j>=size_Eds_direct){cout << "Error! the direct definition do not cover the amount of neighbours, check the neighbour list. Amount of neighbours: "<< Nneighbour.at(i) <<endl; exit(1); }


                exponentd+=Eds_direct.at(i).at(j); //The user must take into account that the different contributions are added together.

            }
        }

    }

    sizeN= Nneighbour.size();
    double exponentp=0.0;
    long long int size_Eps_direct=0;
    j=0;

    for (long long int i = 0; i< sizeN; i++)
    {
        size_Eps_direct=Eps_direct.at(i).size();
        if (size_Eps_direct==0)
        {
            exponentp+=(double)Nneighbour.at(i) * Eps.at(i);
        }
        else
        {
            j = Nneighbour.at(i)-1;
            if (Nneighbour.at(i)==0) {exponentp+= 0.0; }
            else
            {
                if(j>=size_Eps_direct){cout << "Error! the direct definition do not cover the amount of neighbours, check the neighbour list. Amount of neighbours: "<< Nneighbour.at(i) <<endl; exit(1);  }

                exponentp+=Eps_direct.at(i).at(j);
            }
        }
    }

    double rate= fd*exp(-exponentd) *exp(-(-log(0.001))*exp(-exponentp+deltaG)*fr/(exp(-exponentd)*fd) );
    return rate;
}
