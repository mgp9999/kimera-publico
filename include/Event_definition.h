#ifndef EVENT_DEFINITION_H
#define EVENT_DEFINITION_H

#include <vector>
#include <string>
#include "Linked_neighbour.h"


using namespace std;

class Event_definition
{
    public:
        Event_definition();
        virtual ~Event_definition();

        string get_involved_atom_type();
        void set_involved_atom_type(string involved_atom_type_);

        long long int add_distance_neighbours(double distance_);           //returns vector size
        double get_distance_neighbours_by_pos(long long int pos_);         //returns distance to neighbor at position n

        long long int add_type_neighbours(string type_neighbours_);
        string get_type_neighbours_by_pos(long long int pos_);

        long long int add_Ed_neighbours(double Ed_neighbours_);
        double get_Ed_neighbours_by_pos(long long int pos);

        long long int add_Ep_neighbours(double Ep_neighbours_);
        double get_Ep_neighbours_by_pos(long long int pos);

        vector<vector<double>> get_Ed_neighbours_direct();
        long long int add_Ed_neighbours_direct(vector<double> Ed_neighbour_direct_);
        vector<double> get_Ed_neighbours_direct_by_pos(long long int pos_);
        long long int get_size_Ed_neighbours_direct_by_pos(long long int pos_);
        double get_Ed_neighbours_direct_by_posij(long long int posi_,long long int posj_);

        vector<vector<double>> get_Ep_neighbours_direct();
        long long int add_Ep_neighbours_direct(vector<double> Ep_neighbour_direct_);
        vector<double> get_Ep_neighbours_direct_by_pos(long long int pos_);
        long long int get_size_Ep_neighbours_direct_by_pos(long long int pos_);
        double get_Ep_neighbours_direct_by_posij(long long int posi_,long long int posj_);



        long long int add_distance_affected(double distance_affected_);   //returns vector size
        double get_distance_affected_by_pos(long long int pos_);         //returns distance to affected in position n
        vector<double> get_distance_affected();         //returns distance to affected in position n

        long long int add_type_affected(string type_affected_);   //returns vector size
        string get_type_affected_by_pos(long long int pos_);
        vector<string> get_type_affected();


        ////////////// LINK PART

        vector <Linked_neighbour> get_list_linked_neighbour();
        long long int add_to_list_linked_neighbour(Linked_neighbour linked_neighbour_);

        /////////////

        void set_deltaG(double deltaG_);
        double get_deltaG();

        void set_ffd(double ffd_);
        double get_ffd();

        void set_ffp(double ffp_);
        double get_ffp();

        long long int add_max_to_bulk(long long int max_to_bulk_);            //returns size, if it is 0, it has no influence
        vector <long long int> get_max_to_bulk();

        vector <double> get_distance_neighbours();
        vector <string> get_type_neighbours();

        vector <double> get_Ed_neighbours();
        vector <double> get_Ep_neighbours();






    protected:

    private:

        string involved_atom_type;              //Type of atom involved in the event

        vector <double> distance_neighbours;    //Distance to Neighbors that condition the event

        vector <string> type_neighbours;        //type of neighbors that condition the event

        vector <double> Ed_neighbours;           //Dissolution energy with respect to neighbors

        vector <double> Ep_neighbours;           //Precipitation energy relative to neighbors


        vector <vector<double>> Ed_neighbours_direct;

        vector <vector<double>> Ep_neighbours_direct;


        vector <double> distance_affected;       //Distance to atoms that may be affected by the event

        vector <string> type_affected;           //type of things that are affected by the event

        vector <Linked_neighbour> list_linked_neighbour;




        double ffd;                              //fundamental frequency of dissolution
        double ffp;                              //fundamental frequency of precipitation
        double deltaG;                           //deltaG for the event

        vector <long long int> max_to_bulk;                //maximum neighbors with which it is considered to be in bulk

};

#endif // EVENT_DEFINITION_H
