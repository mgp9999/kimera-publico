#ifndef DATA_READER_H
#define DATA_READER_H


#include <stdexcept> //Errors
#include <iostream>
#include <fstream>
#include <string>
#include <stdio.h>
#include <iomanip>
#include <cstdlib>
#include <sstream>
#include <vector>
#include "Control.h"
#include "Sym_equation.h"
#include "Cell.h"
#include "Position.h"
#include "Event_definition.h"
#include "Box.h"
#include "Atom.h"


using namespace std;

class Data_reader
{
    public:
        Data_reader();
        virtual ~Data_reader();

        long long int read_create_xyz_file(string * filename_, Cell * cell);
        long long int read_create_xyz_file_without_corners(string * filename_, Cell * cell, Control * control);
        long long int read_create_cif_file(string * filename_,Control * control, Cell * cell);

        long long int read_input_file(string * filename_, Control * control); //Modify control

        long long int read_input_file_cell(string * filename_, Cell * cell); //Define the cell from which we will create the box

        long long int read_input_file_topography(string * filename_, Box * box); // Once defined the box, we can change it with this method

        long long int read_input_file_mass(string * filename_, Box * box); // set mass to the atoms


        long long int read_input_file_dis_events(string * filename_, Control * control);

        long long int read_kimera_box(string * filename_, Box * box);

        long long int read_remove_add_from_surface(string * filename_, Box * box);  // once defined the surface, this method modify it

        long long int read_modify_element(string * filename_, Box * box);  //Once defined the box, this method modify it's composition


    protected:

    private:


};

#endif // DATA_READER_H
